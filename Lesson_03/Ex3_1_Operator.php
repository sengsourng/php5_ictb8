<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Using Operator in PHP7</title>
</head>
<body>
    <h1>Using Operators</h1>
    <h3>Arimethic Operators</h3>
        <?php
            $x=12;
            $y=20;
            $Sum=$x+$y;
            $Sub=$x-$y;
            $Div=$x/$y;
            $Mod=$x%$y;

            echo "Sum $x + $y=". $Sum; 
            echo "<br>";
            echo "Sub $x - $y=". $Sub;
            echo "<br>";
            echo "Div $x / $y=". $Div;
            echo "<br>";
            echo "Mod $x % $y=". $Mod;            

        ?>

</body>
</html>