<?php
      $myNum=0;
            $myNum= isset($_POST['myNum']) ? $_POST['myNum'] : '';//$_POST['fname'];
            // $lname=isset($_POST['lname']) ? $_POST['lname'] : ''; //$_POST['lname'];
            // $gender=isset($_POST['gender']) ? $_POST['gender'] : ''; //$_POST['gender'];
            // $favcolor=isset($_POST['color']) ? $_POST['color'] : ''; //$_POST['color'];       

        ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>
  
<div class="container">
  <div class="row">
    <div class="col-sm-7">
      <h3>Using Loop Statement</h3>
        <form action="loop.php" method="POST">
            <div class="form-group">
                <label for="fname">បញ្ចូល​មេលេខ</label>
                <input type="text" class="form-control" id="fname" placeholder="បញ្ចូល​មេលេខ Ex. 5" name="myNum" value="<?php echo $myNum; ?>">
            </div>
            
           
            <button type="submit" class="btn btn-success">គណនា</button>
        </form>
      

    </div>
    
    <div class="col-sm-5">
      <h3>Result</h3>     
        
    <?php 
        for($i=1;$i<=10;$i++){
           ?>
                <h2><?php echo @$myNum; ?> x <?php echo $i; ?>= <?php echo @$myNum*$i; ?></h2>
           <?php
        }     

    ?>

<!-- Using Do while -->
<h1>Result Do ...while Loop</h1>
    <?php 
    $i=1;
    do{
        echo "<h3> $myNum x $i =". @$myNum * $i ."</h3>";
        $i++;
    }while($i<=10);

    ?>
<h2>While Loop</h2>
        
<?php 
    $i=1;
   while($i<=10){
    echo "<h3> $myNum x $i =". @$myNum * $i ."</h3>";
    $i++;
   }
?>

    </div> 
  
  
  </div>
</div>

</body>
</html>
