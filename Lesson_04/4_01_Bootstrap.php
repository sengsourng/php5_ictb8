<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>My First Control Statement</h1>
  <p>Start Using Control Statement</p> 
</div>
  
<div class="container">
  <div class="row">
    <div class="col-sm-8">
      <h3>Complete Form</h3>
        <form action="4_01_Bootstrap.php" method="POST">
            <div class="form-group">
                <label for="fname">First Name</label>
                <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" placeholder="Last Name" name="lname">
            </div>

            <div class="form-group">
                <label for="gender">Gender</label>
                <select class="form-control" id="gender" name="gender">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    
                </select>
            </div>

            <div class="form-group">
                <label for="Color">Color</label>
                <select class="form-control" id="Color" name="color">
                    <option value="Red">Red</option>
                    <option value="Blue">Blue</option>
                    <option value="Green">Green</option>                    
                </select>
            </div>
           
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
      

    </div>
    
    <div class="col-sm-4">
      <h3>Result</h3>        
        <?php
            $fname=$_POST['fname'];
            $lname=$_POST['lname'];
            $gender=$_POST['gender'];
            $color=$_POST['color'];
        
        ?>
        <h3 style="color: <?php echo $color; ?>">Name : <?php echo $fname ." ". $lname; ?></h3>
        <h3 style="color: <?php echo $color; ?>">Gender : <?php echo $gender; ?></h3>
        <h2>Color : <?php echo $color; ?></h2>
    </div> 
  
  
  </div>
</div>

</body>
</html>
