<?php 
    $name="Long Dara";
    $color="Yellow";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample Lesson 01_08</title>
</head>
<body>
    <h2>What's your name and your favorit color?</h2>
    <h1>My name is 
        <span style="color:<?php echo $color; ?>">
            <?php echo $name; ?>
        </span>  
        and my favorit color is 
        <span style="color:<?php echo $color; ?>">
            <?php echo $color; ?>
        </span>
    </h1>
</body>
</html>