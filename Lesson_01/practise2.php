<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Register User</h2>
  <form action="practise2.php" method="POST">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="fullname">Full Name:</label>
                <input type="text" class="form-control" id="fullname" placeholder="Enter Fullname" name="full_name">
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>


            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
            </div>
            
            
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="remember"> Remember me
                 </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        
        
        </div>
        <div class="col-md-6">
            <img src="https://media.istockphoto.com/photos/modern-keyboard-with-blue-online-registration-button-picture-id931524202?k=6&m=931524202&s=612x612&w=0&h=fkVt-h-ougSVdPFihgTQDNor0eCVbIWPmg6vwbVUuq0=" alt="">
            <!-- <img src="../img/111.webp" alt=""> -->
       
        </div>
    
        <div class="col-md-12">
            <!-- Result -->
        <h1>Result Submit</h1>
        <hr>
        <?php 
            $full_name="";
            $email="";
            $password="";
            $remember=0; 

            $full_name=$_POST['full_name'];
            $email=$_POST['email'];
            @$password=$_POST['password'];
            $remember=$_POST['remember'];

        ?>

        <h3>Full Name : <span><?php echo $full_name; ?></span></h3>
        <h3>Email : <span><?php echo $email; ?></span></h3>
        <h3>Password : <span><?php echo $password; ?></span></h3>
        <h3>Remember : <span><?php echo $remember; ?></span></h3>
        </div>

    
    </div>



  </form>
</div>

</body>
</html>