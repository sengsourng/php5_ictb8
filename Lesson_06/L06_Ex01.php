<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<h2>បង្កើត​ Array: Cars</h2>

    <?php
        // បង្កើត Array ឈ្មោះ​ថា​ $cars
        $cars=array("Volvo","BMW","Toyota");
        echo "I like ". $cars[0] ." , ". $cars[1]. " , ". $cars[2].".";

    ?>
<h2>បង្កើត​ Array Students</h2>
    <?php 
        $students=array(); //បង្កើត​ Array students

        //Assign value to array 
        $students[0]="Long Dara";
        $students[1]="Keo Thida";
        $students[2]="Sam Dara";
        $students[3]="Meas Dara";

        //បង្ហាញតម្លៃ Array : Keo Thida
        // echo $students[1];

        //បង្ហាញតម្លៃទាំងអស់​របស់​ Array $students

        for($i=0;$i<count($students);$i++){
            echo $students[$i];
            echo "<br>";
        }

        


    
    ?>

</body>
</html>