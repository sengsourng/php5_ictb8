<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Associative Array</title>

    <style>
        span{
            color:blue;
        }

        .myClass{
            color: green;
        }
    </style>
</head>
<body>

    <h1>Using Associative Array</h1>
    <?php 
        $person=array(
                "id"        => 01,
                "name"      => "Long Dara",
                "sex"       => "Male",
                "occupation" => "Teacher",
                "age"       => 40,
                "address"   => "Phnom Penh"
                );         
     
    ?>
    <!-- បង្ហាញតម្លៃរបស់​ Associative Array -->
    <h3>ID : <span><?php echo $person['id']; ?></span></h3>
    <h3>Name : <span><?php echo $person['name']; ?></span></h3>
    <h3>Sex : <span><?php echo $person['sex']; ?></span></h3>
    <h3>Occupation : <span><?php echo $person['occupation']; ?></span></h3>
    <h3>Age : <span><?php echo $person['age']; ?></span></h3>
    <h3>Address : <span><?php echo $person['address']; ?></span></h3>
    

    <h2>Title 01  <span class="myClass"> Span </span> </h2> 

    <?php
        foreach($person as $key=>$p){
            echo  $p;
            echo "<br>";
        }
    
    ?>

</body>
</html>