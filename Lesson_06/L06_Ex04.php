<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Product Information</h2>


<?php
$cars = array (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15),
  array("Tesla",27,25)
);
    
// for ($row = 0; $row < 4; $row++) {
//   echo "<p><b>Row number $row</b></p>";
//   echo "<ul>";
//   for ($col = 0; $col < 3; $col++) {
//     echo "<li>".$cars[$row][$col]."</li>";
//   }
//   echo "</ul>";
// }

?>

    <table>
        <tr>
            <th>Name</th>
            <th>Stock</th>
            <th>Sold</th>
        </tr>

    <?php foreach($cars as $key=>$car){ ?>  
        <tr>
            <td><?php echo $car[0]; ?></td>
            <td><?php echo $car[1]; ?></td>
            <td><?php echo $car[2]; ?></td>
        </tr>

    <?php } ?> 

    </table>
</body>
</html>


